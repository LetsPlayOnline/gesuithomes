package eu.letsplayonline.geSuitHomes;

import eu.letsplayonline.geSuitHomes.commands.*;
import eu.letsplayonline.geSuitHomes.listeners.HomesListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class geSuitHomes extends JavaPlugin {
	public static geSuitHomes instance;

	@Override
	public void onEnable() {
		instance = this;
		registerListeners();
		registerChannels();
		registerCommands();
	}
	
	private void registerCommands() {
		getCommand("sethome").setExecutor(new SetHomeCommand());
		getCommand("home").setExecutor(new HomeCommand());
		getCommand("delhome").setExecutor(new DelHomeCommand());
		getCommand("homes").setExecutor(new HomesCommand());
		getCommand("importhomes").setExecutor(new ImportHomesCommand());
	}

	private void registerChannels() {
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "geSuitHomes");
	}

	private void registerListeners() {
		getServer().getPluginManager().registerEvents(
				new HomesListener(), this);
	}


}
